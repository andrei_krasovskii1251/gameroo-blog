<?php
/**
 * modernize functions and definitions.
 *
 * @package modernize
 * @author Takuma Misumi
 * @link http://blog.mismithportfolio.com/
 * @license GPLv2 or later
 */

if ( ! function_exists( 'modernize_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function modernize_setup() {
  /*
   * Make theme available for translation.
   * Translations can be filed in the /languages/ directory.
   */
  load_theme_textdomain( 'modernize', get_template_directory() . '/languages' );

  // Add default posts and comments RSS feed links to head.
  add_theme_support( 'automatic-feed-links' );

  /*
   * Let WordPress manage the document title.
   * By adding theme support, we declare that this theme does not use a
   * hard-coded <title> tag in the document head, and expect WordPress to
   * provide it for us.
   */
  add_theme_support( 'title-tag' );

  /*
   * Enable support for Post Thumbnails on posts and pages.
   */
  add_theme_support( 'post-thumbnails' );

  /*
   * This theme uses wp_nav_menu() in one location.
   */
  register_nav_menus( array(
    'primary' => esc_html__( 'Primary', 'modernize' ),
    'social'  => esc_html__( 'Social Links Menu', 'modernize' ),
  ) );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comments__list',
    'gallery',
    'caption',
  ) );

  /*
   * Enable support for Post Formats.
   */
  add_theme_support( 'post-formats', array(
    'aside',
    'image',
    'video',
    'quote',
    'link',
  ) );

  // Add theme support for Custom Logo.
  add_theme_support( 'custom-logo', array(
    'width'       => 250,
    'height'      => 250,
    'flex-width'  => true,
  ) );

  // Set up the WordPress core custom background feature.
  add_theme_support( 'custom-background', apply_filters( 'modernize_custom_background_args', array(
    'default-color' => 'ffffff',
    'default-image' => '',
  ) ) );

  /*
   * Enable support editor-style on WordPress dashboard.
   */
  add_editor_style( array(
    'editor-style.css',
  ) );
}
endif;
add_action( 'after_setup_theme', 'modernize_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 */
function modernize_content_width() {
  $GLOBALS['content_width'] = apply_filters( 'modernize_content_width', 700 );
}
add_action( 'after_setup_theme', 'modernize_content_width', 0 );

/**
 * Register widget area.
 */
function modernize_widgets_init() {
  register_sidebar( array(
    'name'          => esc_html__( 'Sidebar', 'modernize' ),
    'id'            => 'sidebar-1',
    'description'   => esc_html__( 'Add widgets here.', 'modernize' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="widget__title">',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'modernize_widgets_init' );

/*
 * Register excerpt length.
 */
function custom_excerpt_length( $length ) {
	return 120;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
  return '[&hellip;]';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

/**
 * Enqueue scripts and styles.
 */
function modernize_scripts() {
  $url = get_template_directory_uri();
  $theme   = wp_get_theme();
  $version = $theme->get( 'Version' );

  wp_enqueue_style( 'modernize-style', $url . '/style.css' );

  if ( is_child_theme() ) {
    wp_enqueue_style( get_stylesheet(), get_stylesheet_uri(), array( 'modernize-style' ), $version);
  }

  wp_enqueue_script( 'modernize-main', $url . '/js/bundle.js', array('jquery'), $version, true );

  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'modernize_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

add_action('add_meta_boxes', 'my_extra_fields', 1);

function my_extra_fields() {
  add_meta_box( 'extra_fields', 'Aditional fields', 'extra_fields_box_func', 'post', 'normal', 'high'  );
}

function extra_fields_box_func( $post ){
    wp_enqueue_media(); ?>
    <script>
        jQuery(document).ready(function(){

            jQuery('.add_images').click(function(e) {
                e.preventDefault();
                var image = wp.media({
                    title: 'Upload image',
                    multiple: true
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first();
                        // We convert uploaded_image to a JSON object to make accessing it easier
                        // Output to the console uploaded_image
                        var array_object = image.state().get('selection').toJSON();

                        var image_url_array = [];
                        if(array_object.length > 7) {

                            var image_urls = '';
                        }
                        else {
                            for(var i = 0; i < array_object.length; i++) {
                                image_url_array.push(array_object[i].url);
                            }

                            var image_urls = JSON.stringify(image_url_array);
                        }
                        // Let's assign the url value to the input field
                        jQuery('.input_images').val(image_urls);
                    });
            });
        });
    </script>
    <?php $image = (get_post_meta($post->ID, 'image', 1)); 
      if(isset($image) && !empty($image)):
    ?>
    <?php $image = json_decode($image); ?>
    <img src="<?php echo $image[0]; ?>" style="width:281px; height:136px">
    <?php endif;  ?>
    <p>
        <button class="button button-primary add_images">Add image</button>
        <input class="input_images" type="hidden" name="extra[image]" value="">
    </p>
  <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
  <input name="save" type="submit" class="button button-primary button-large" value="Save">
  <?php
}

add_action('save_post', 'my_extra_fields_update', 0);
/* Сохраняем данные, при сохранении поста */
function my_extra_fields_update($post_id ) {

    if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false; // проверка
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // если это автосохранение
    if ( !current_user_can('edit_post', $post_id) ) return false; // если юзер не имеет право редактировать запись

    if( !isset($_POST['extra']) ) return false;

   
    foreach( $_POST['extra'] as $key=>$value ){
        if( empty($value) && $key != 'image' ){
            delete_post_meta($post_id, $key); // удаляем поле если значение пустое
            continue;
        }
        if($key == 'image' && !empty($value)){
        update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
            
     }
        if($key !='image')
            update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
    }
    return $post_id;
}