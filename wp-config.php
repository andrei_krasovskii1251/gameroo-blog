<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'gameroo_blog');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '23L-lAZX~h7:R{`NTs+3dX:8Gzo^{VU+0-M{Di;ANpup+cWrjWaQJo!L=%l8H0`r');
define('SECURE_AUTH_KEY',  '#9+Y%3lR967bSJNlRIBdU,gL^sL$g=RSQHeV`^`+z>W3cJ=1Y<RvIw868qf>{y,#');
define('LOGGED_IN_KEY',    'b+MIJlm4/y&fL+zd%%.wa)]*ekxbf6q5!$!];Kn2X% (*Fc5?c,2*=}tPIc@?Pve');
define('NONCE_KEY',        '@;cS2$3]|;Z%,0)wTC$XXbr|j[Axvk4^GU]:8Q)5iH8H|y}6{bG7.5%8bb$&&]Z:');
define('AUTH_SALT',        '&Np1%InEn~c>TUBJc=-,LTr}VMgK,J[y{|,#k?%?N&SJa4ZGW$8Mnn_7|*m2Kgjr');
define('SECURE_AUTH_SALT', '&4T@/^0K96Vo<DZ1$TKK}sm8qU6W=_#xDx*cM@&y<e]A-}j.jZi?Zc!Unmf!,a(2');
define('LOGGED_IN_SALT',   'q5n8d4(38v4Ec(=QKei%_k<rh#.YYO=S6;SU$Mv_.|Ql?ikEe]tDmIv4[)[X-JF9');
define('NONCE_SALT',       '25Xl-4,*l]>p:rWCS +LC5iI8u}B=GR@ZK]Gijz0):!S:zC`w>>:#X1T,~`*I3TL');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
define('FS_METHOD', 'direct');
